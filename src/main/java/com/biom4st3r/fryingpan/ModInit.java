package com.biom4st3r.fryingpan;

import java.util.List;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArmorMaterials;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ToolMaterials;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;

public class ModInit implements ModInitializer {
	public static final String MODID = "fryingpan";

	public static SoundEvent FRYING_PAN_HIT_WEAK;
	public static SoundEvent FRYING_PAN_HIT_STORNG;
	public static Item ITEM_FRYING_PAN;
	public static Block BLOCK_FRYING_PAN;
	public static Item ITEM_BURNT_FOOD;
	public static BlockEntityType<FryingPanBlockEntity> BE_FRYING_PAN;

	@SuppressWarnings("unused")
	private static void playSound(World world, PlayerEntity pe, List< SoundEvent> event)
	{
		// ChunkSection
		event.forEach((sound)->world.playSound(null, pe.getBlockPos(), sound, SoundCategory.PLAYERS, 1.0F, 1.0F));
	}

	@Override
	public void onInitialize() {

		Identifier id = new Identifier(ModInit.MODID, "item.fryingpan.hit0");
		FRYING_PAN_HIT_WEAK = Registry.register(Registry.SOUND_EVENT, id, new SoundEvent(id));

		id = new Identifier(MODID,"item.fryingpan.hit1");
		FRYING_PAN_HIT_STORNG = Registry.register(Registry.SOUND_EVENT, id, new SoundEvent(id));
		
		id = new Identifier(MODID,"fryingpan");
		BLOCK_FRYING_PAN = Registry.register(Registry.BLOCK, id, new FryingPanBlock(AbstractBlock.Settings.copy(Blocks.IRON_BLOCK).breakInstantly().nonOpaque()));
		ITEM_FRYING_PAN = Registry.register(
			Registry.ITEM, id, 
			new FryingPanItem(8.5F,-3.4F,ArmorMaterials.IRON.getProtectionAmount(EquipmentSlot.HEAD),new FabricItemSettings()
				.fireproof()
				.equipmentSlot((stack)->EquipmentSlot.HEAD)
				.maxCount(1).group(ItemGroup.TOOLS)
				.maxDamage(ToolMaterials.IRON.getDurability()+200)
				// .soundEvent(Sound.STRONG, (world,player)->playSound(world,player,Lists.newArrayList(FRYING_PAN_HIT_STORNG)))
				// .soundEvent(Sound.WEAK, (world,player)->playSound(world,player,Lists.newArrayList(FRYING_PAN_HIT_WEAK)))
				// .soundEvent(Sound.CRITIAL, (world,player)->playSound(world, player, Lists.newArrayList(FRYING_PAN_HIT_STORNG,SoundEvents.ENTITY_PLAYER_ATTACK_CRIT)))
				));
		BE_FRYING_PAN = Registry.register(Registry.BLOCK_ENTITY_TYPE, id, BlockEntityType.Builder.create(()->new FryingPanBlockEntity(), BLOCK_FRYING_PAN).build(null));
		ITEM_BURNT_FOOD = Registry.register(Registry.ITEM, new Identifier(MODID, "burntitem"), new Item(new Item.Settings()));

		// FuelRegistry.INSTANCE.add(ITEM_BURNT_FOOD, 100);
	}
}
