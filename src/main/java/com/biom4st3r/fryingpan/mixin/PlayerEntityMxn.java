package com.biom4st3r.fryingpan.mixin;

import com.biom4st3r.fryingpan.ModInit;

import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.sound.*;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMxn {

    @Unique boolean fryingpan$shouldMute = false;
    @Unique final float fringpan$pitch = 1.0F;

    @Inject(
        at = @At(
            value="INVOKE",
            target="net/minecraft/world/World.playSound(Lnet/minecraft/entity/player/PlayerEntity;DDDLnet/minecraft/sound/SoundEvent;Lnet/minecraft/sound/SoundCategory;FF)V",
            ordinal = 2,
            shift = Shift.BEFORE),
        method = "attack")
    public void fryingpan$Before_ENTITY_PLAYER_CRIT(Entity e,CallbackInfo ci)
    {
        PlayerEntity pe = (PlayerEntity)(Object)this;
        if(pe.getMainHandStack().getItem() == ModInit.ITEM_FRYING_PAN)
        {
            pe.world.playSound(null, e.getBlockPos(), ModInit.FRYING_PAN_HIT_STORNG, SoundCategory.PLAYERS, 1.0F, fringpan$pitch);
        }
    }    

    @Inject(
        at = @At(
            value="INVOKE",
            target="net/minecraft/world/World.playSound(Lnet/minecraft/entity/player/PlayerEntity;DDDLnet/minecraft/sound/SoundEvent;Lnet/minecraft/sound/SoundCategory;FF)V",
            ordinal = 3,
            shift = Shift.BEFORE),
        method = "attack")
    public void fryingpan$Before_ENTITY_PLAYER_ATTACK_STRONG(Entity e,CallbackInfo ci)
    {
        PlayerEntity pe = (PlayerEntity)(Object)this;
        if(pe.getMainHandStack().getItem() == ModInit.ITEM_FRYING_PAN)
        {
            pe.world.playSound(null, e.getBlockPos(), ModInit.FRYING_PAN_HIT_STORNG, SoundCategory.PLAYERS, 1.0F, fringpan$pitch);
            fryingpan$shouldMute = true;
        }
    }

    @ModifyArg(
        at = @At(
            value="INVOKE",
            target="net/minecraft/world/World.playSound(Lnet/minecraft/entity/player/PlayerEntity;DDDLnet/minecraft/sound/SoundEvent;Lnet/minecraft/sound/SoundCategory;FF)V",
            ordinal = 3),
        index = 6,
        method = "attack")
    public float fryingpan$mute_Attack_Strong(float volume)
    {
        if(fryingpan$shouldMute) 
        {
            fryingpan$shouldMute = false;
            return 0.0F;
        }
        return volume;
    }
    
    @Inject(
        at = @At(
            value="INVOKE",
            target="net/minecraft/world/World.playSound(Lnet/minecraft/entity/player/PlayerEntity;DDDLnet/minecraft/sound/SoundEvent;Lnet/minecraft/sound/SoundCategory;FF)V",
            ordinal = 4,
            shift = Shift.BEFORE),
        method = "attack")
    public void fryingpan$Before_ENTITY_PLAYER_ATTACK_WEAK(Entity e,CallbackInfo ci)
    {
        PlayerEntity pe = (PlayerEntity)(Object)this;
        if(pe.getMainHandStack().getItem() == ModInit.ITEM_FRYING_PAN)
        {
            pe.world.playSound(null, e.getBlockPos(), ModInit.FRYING_PAN_HIT_WEAK, SoundCategory.PLAYERS, 1.0F, fringpan$pitch);
            fryingpan$shouldMute = true;
        }
    }
    
    @ModifyArg(
        at = @At(
            value="INVOKE",
            target="net/minecraft/world/World.playSound(Lnet/minecraft/entity/player/PlayerEntity;DDDLnet/minecraft/sound/SoundEvent;Lnet/minecraft/sound/SoundCategory;FF)V",
            ordinal = 4),
        index = 6,
        method = "attack")
    public float fryingpan$mute_Attack_Weak(float volume)
    {
        if(fryingpan$shouldMute) 
        {
            fryingpan$shouldMute = false;
            return 0.0F;
        }
        return volume;
    }
}
