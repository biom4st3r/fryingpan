package com.biom4st3r.fryingpan;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;
import net.minecraft.world.World;

import com.mojang.datafixers.util.Pair;

import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.mesh.Mesh;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;

public class FryingPanModel implements UnbakedModel, BakedModel, FabricBakedModel {

    private UnbakedModel unbaked;
    private BakedModel baked;
    private Mesh mesh;

    public FryingPanModel(UnbakedModel model)
    {
        this.unbaked = model;
    }

    private Mesh meshify(World world,BlockState state,BlockPos pos)
    {
        MeshBuilder builder = RendererAccess.INSTANCE.getRenderer().meshBuilder();
        QuadEmitter emitter = builder.getEmitter();
        Random random = new Random();
        int color = 0xFF000000 | MinecraftClient.getInstance().getBlockColors().getColor(state, world, pos, 0xFF);
        for(Direction dir : QUAD_DIRECTIONS)
        {
            for(BakedQuad quad : this.baked.getQuads(state, dir, random))
            {
                emitter.fromVanilla(quad.getVertexData(), 0, false);
                if(quad.hasColor()) emitter.colorIndex(0);
                else emitter.colorIndex(-1);
                emitter.cullFace(dir);
                emitter.spriteColor(0, color, color, color, color);
                emitter.emit();
            }
        }
        return builder.build();
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
            Supplier<Random> randomSupplier, RenderContext context) {
        if(mesh == null)
        {
            mesh = this.meshify((World) blockView, state, pos);
        }

        context.meshConsumer().accept(mesh);
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
        if(mesh == null) context.fallbackConsumer().accept(baked);
        else context.meshConsumer().accept(mesh);
    }

    @Override
    public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
        return this.baked.getQuads(state, face, random);
    }

    @Override
    public boolean useAmbientOcclusion() {
        return this.baked.useAmbientOcclusion();
    }

    @Override
    public boolean hasDepth() {
        return this.baked.hasDepth();
    }

    @Override
    public boolean isSideLit() {
        return this.baked.isSideLit();
    }

    @Override
    public boolean isBuiltin() {
        return this.baked.isBuiltin();
    }

    @Override
    public Sprite getSprite() {
        return this.baked.getSprite();
    }

    @Override
    public ModelTransformation getTransformation() {
        return this.baked.getTransformation();
    }

    @Override
    public ModelOverrideList getOverrides() {
        return this.baked.getOverrides();
    }

    @Override
    public Collection<Identifier> getModelDependencies() {
        return this.unbaked.getModelDependencies();
    }

    @Override
    public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter,
            Set<Pair<String, String>> unresolvedTextureReferences) {
        return this.unbaked.getTextureDependencies(unbakedModelGetter, unresolvedTextureReferences);
    }

    static final Direction[] QUAD_DIRECTIONS = Util.make(()->
    {
        int size = Direction.values().length;
        Direction[] QUAD_DIR = Arrays.copyOf(Direction.values(),size+1);
        QUAD_DIR[size] = null;
        return QUAD_DIR;
    });

    @Override
    public BakedModel bake(ModelLoader loader, Function<SpriteIdentifier, Sprite> textureGetter,
            ModelBakeSettings rotationContainer, Identifier modelId) {
                
        this.baked = this.unbaked.bake(loader, textureGetter, rotationContainer, modelId);
        return this;
    }

    
}
