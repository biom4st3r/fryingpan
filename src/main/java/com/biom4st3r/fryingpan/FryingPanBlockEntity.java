package com.biom4st3r.fryingpan;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.tag.BlockTags;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import com.google.common.collect.Sets;

import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.fabricmc.fabric.api.tag.TagRegistry;

public class FryingPanBlockEntity extends BlockEntity implements Tickable,BlockEntityClientSerializable {
    static Set<Block> hotStuff = Sets.newHashSet(Blocks.MAGMA_BLOCK,Blocks.LAVA,Blocks.TORCH,Blocks.SOUL_TORCH,Blocks.WALL_TORCH,Blocks.SOUL_WALL_TORCH);
    static Tag<Block> veryHot = TagRegistry.block(new Identifier("burninator", "very_hot"));
    static Tag<Block> hot = TagRegistry.block(new Identifier("burninator", "hot"));
    public FryingPanBlockEntity() {
        super(ModInit.BE_FRYING_PAN);
    }


    boolean isHot(BlockState state)
    {
        if(state.isIn(BlockTags.CAMPFIRES) && state.get(Properties.LIT))
        {
            return true;
        } else if(state.isIn(BlockTags.FIRE))
        {
            return true;
        }
        return hotStuff.contains(state.getBlock()) | veryHot.contains(state.getBlock()) | hot.contains(state.getBlock());
    }

    SimpleInventory inventory = new SimpleInventory(1);
    ItemStack currentStack = ItemStack.EMPTY;
    SmeltingRecipe recipe = null;
    int remainingTicks = -1;
    int initialTicks = -1;

    private SmeltingRecipe assignRecipe() {
        inventory.setStack(0, currentStack);
        SmeltingRecipe recipe = this.world.getRecipeManager().getFirstMatch(RecipeType.SMELTING, inventory, this.world).get();
        return recipe;
    }

    public void startCooking()
    {
        this.recipe = this.assignRecipe();
        float addtionalTimeMod = (this.currentStack.getCount()-1)*0.2F;
        if(this.currentStack.getCount() > 1)
        {
            initialTicks = remainingTicks = (int)((recipe.getCookTime()*0.55F) * Math.max(1, addtionalTimeMod));
        }
        else
        {
            initialTicks = remainingTicks = (int) (recipe.getCookTime() * 0.55F) ;
        }
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        tag.putInt("dmg", this.damage);
        tag.put("item",currentStack.toTag(new CompoundTag()));
        if(!currentStack.isEmpty())
        {
            tag.putInt("remaintime", remainingTicks);
            tag.putInt("starttime", initialTicks);
        }
        return super.toTag(tag);
    }
    
    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        this.damage = tag.getInt("dmg");
        this.currentStack = ItemStack.fromTag(tag.getCompound("item"));
        if(!this.currentStack.isEmpty() && this.world != null)
        {
            this.recipe = assignRecipe();
            this.remainingTicks = tag.getInt("remaintime");
            this.initialTicks = tag.getInt("starttime");
        }
        super.fromTag(state, tag);
    }

    @Override
    public void fromClientTag(CompoundTag tag) {
        this.fromTag(this.getCachedState(), tag);
    }
    @Override
    public CompoundTag toClientTag(CompoundTag tag) {
        this.toTag(tag);
        return tag;
    }

    int damage = 0;
    @Override
    public void tick() {
        if(!isHot(this.world.getBlockState(this.pos.down())))
        {
            if(!this.world.isClient && !this.currentStack.isEmpty())
            {
                if ((System.nanoTime()/1000000 & 31) == 31)
                {
                    this.sync();
                }
            }
            return;
        }

        if(currentStack.isEmpty())
        {
            List<ItemEntity> items = this.world.getEntitiesByType(EntityType.ITEM, new Box(this.pos).contract(0.75F), (i)->
            {
                inventory.setStack(0, i.getStack());
                Optional<SmeltingRecipe> o = this.world.getRecipeManager().getFirstMatch(RecipeType.SMELTING, inventory, this.world);
                return o.isPresent() && (i.getStack().isFood() || o.get().getOutput().isFood());
            });
            if(items.isEmpty()) return;
            ItemEntity item = items.get(0);
            if(item.removed) return;
            item.remove();
            this.currentStack = item.getStack();
            inventory.setStack(0, this.currentStack);
            Optional<SmeltingRecipe> recipe = this.world.getRecipeManager().getFirstMatch(RecipeType.SMELTING, inventory, this.world);
            if(!recipe.isPresent())
            {
                currentStack = ItemStack.EMPTY;
                return;
            }

            startCooking();
        }
        else // !currentstack.isEmpty()
        {
            if(recipe == null)
            {
                startCooking();
            }
            if((this.world.random.nextInt() & 0x7) == 0x7)
            {
                this.world.playSound(null, this.pos, SoundEvents.BLOCK_FURNACE_FIRE_CRACKLE, SoundCategory.BLOCKS, 1.0F, 1.0F);

                for(int i = 0; i < 4;i++) world.addParticle(ParticleTypes.SMOKE, this.pos.getX()+0.5F, this.pos.getY()+0.1F, this.pos.getZ()+0.5F, 0.0D, 5.0E-4D, 0.0D);
            }
            if((this.world.random.nextInt() & 127) == 127)
            {
                this.damage+=1;
            }
            if(remainingTicks > 0) {
                remainingTicks--;
                if(!this.world.isClient && (remainingTicks & 0xF) == 0xF)
                {
                    this.sync();
                }
            }
            else
            {
                ItemStack is = (world.random.nextInt(512)&511) == 0 ? new ItemStack(ModInit.ITEM_BURNT_FOOD) : this.recipe.getOutput().copy();
                if(is.getItem() == ModInit.ITEM_BURNT_FOOD) is.getOrCreateTag().putString("hostitem", Registry.ITEM.getId(this.recipe.getOutput().copy().getItem()).toString());
                is.setCount(this.currentStack.getCount());
                ItemEntity item = new ItemEntity(this.world, this.pos.getX()+0.5F, this.pos.getY()+0.5F, this.pos.getZ()+0.5F, is);
                item.setToDefaultPickupDelay();
                this.currentStack = ItemStack.EMPTY;

                this.recipe = null;
                this.initialTicks = this.remainingTicks = -1;

                this.world.spawnEntity(item);
            }
        }
    }

    public ItemEntity dropItem(World world,BlockPos pos)
    {
        ItemStack stack = new ItemStack(ModInit.ITEM_FRYING_PAN);
        stack.setDamage(this.getDamage());
        ItemEntity entity = new ItemEntity(world, pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, stack);
        entity.setToDefaultPickupDelay();
        world.spawnEntity(entity);
        if(!this.currentStack.isEmpty())
        {
            entity = new ItemEntity(world, pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, this.currentStack);
            entity.setToDefaultPickupDelay();
            world.spawnEntity(entity);
            return entity;
        }
        return null;
    }

	public void setDamage(int damage) {
        this.damage = damage;
    }
    public int getDamage()
    {
        return this.damage;
    }
}
