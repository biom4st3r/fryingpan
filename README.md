# Frying Pan
Adds a Frying pan for Cooking.

 

![Imgur](https://i.imgur.com/c6AartD.png)

*To use your new Frying Pan* just place it on top of a source of heat and drop your food in.

Attributions:
* Weak hit BONK sound from bart@https://opengameart.org/content/metal-clang-sounds

* Strong hit BONK sound from fryingpan@my house
* Metal texture derived from TRaK@https://opengameart.org/content/metal-panel-dark-brown-no-nails
* Inspiration from Mod gdude@Fabricord